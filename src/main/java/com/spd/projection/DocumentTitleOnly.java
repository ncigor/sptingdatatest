package com.spd.projection;

import org.springframework.beans.factory.annotation.Value;

/**
 * Created by Igor on 16.12.2016.
 */
public interface DocumentTitleOnly {
    String getTitle();

    //prefixed with target
    @Value("#{target.id} :: #{target.title} ")
    String getTitleWithId();
}
