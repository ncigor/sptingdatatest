package com.spd.entity;

import javax.persistence.*;
import java.lang.annotation.Documented;
import java.util.List;

/**
 * Created by Igor on 11.12.2016.
 */
@Entity
@Table(name = "AUTHOR")
@NamedQuery(name = "Author.findByNamedQuery",
        query = "select a from Author a where a.name = ?1")
@NamedStoredProcedureQuery(name = "Author.plus1", procedureName = "plus1inout", parameters = {
        @StoredProcedureParameter(mode = ParameterMode.IN, name = "arg", type = Integer.class),
        @StoredProcedureParameter(mode = ParameterMode.OUT, name = "res", type = Integer.class) })
public class Author {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "author")
    private List<Document> documents;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Document> getDocuments() {
        return documents;
    }

    public void setDocuments(List<Document> documents) {
        this.documents = documents;
    }

    @Override
    public String toString() {
        return "Author: {id : " + id + ", name: " + name + "}";
    }
}
