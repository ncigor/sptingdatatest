package com.spd;

import com.spd.jpa.AuthorRepository;
import com.spd.jpa.DocumentRepository;
import com.spd.testdata.TestDataGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Igor on 13.12.2016.
 */
@RestController
public class TestController {
    @Autowired
    private TestDataGenerator testDataGenerator;

    @Autowired
    AuthorRepository authorRepository;

    @Autowired
    DocumentRepository documentRepository;

    @RequestMapping("/generateTestData")
    ResponseEntity<Void> generateTestData(){
        testDataGenerator.generateData();
        return ResponseEntity.noContent().build();
    }


}
