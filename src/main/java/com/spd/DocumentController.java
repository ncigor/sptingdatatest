package com.spd;

import com.spd.entity.Author;
import com.spd.entity.Document;
import com.spd.jpa.AuthorRepository;
import com.spd.jpa.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.hateoas.PagedResources;
import java.util.List;

/**
 * Created by Igor on 14.12.2016.
 */
@RestController
public class DocumentController {
    @Autowired
    DocumentRepository repository;

    @RequestMapping(value = "/authors", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    HttpEntity<PagedResources<Document>> persons(Pageable pageable,
                                       PagedResourcesAssembler assembler) {

        Page<Document> persons = repository.findAll(pageable);
        return new ResponseEntity<>(assembler.toResource(persons), HttpStatus.OK);
    }
}
