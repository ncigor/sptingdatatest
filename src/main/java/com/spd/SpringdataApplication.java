package com.spd;

import com.spd.jpa.AuthorRepository;
import com.spd.jpa.DocumentRepository;
import com.spd.testdata.TestDataGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@EnableJpaRepositories(basePackages = "com.spd.jpa")
@ComponentScan(basePackages = "com.spd")
@EnableSpringDataWebSupport
public class SpringdataApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringdataApplication.class, args);
	}
}
