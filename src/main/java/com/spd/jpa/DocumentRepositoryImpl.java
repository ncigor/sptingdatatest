package com.spd.jpa;

import com.spd.entity.Document;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by Igor on 13.12.2016.
 */
//@Component
class DocumentRepositoryImpl implements DocumentRepositoryCustom{
    @Override
    public Optional<Document> myCustomMethodForADoc(){
        Document document = new Document();
        document.setTitle("------- Custom ----");
        return Optional.of(document);
    }
}
