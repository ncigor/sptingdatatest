package com.spd.jpa;

import com.spd.entity.Document;
import com.spd.projection.DocumentTitleOnly;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import javax.print.Doc;
import java.util.List;

/**
 * Created by Igor on 11.12.2016.
 */
//@NoRepositoryBean
public interface DocumentRepository extends JpaRepository<Document, String>, DocumentRepositoryCustom {
//    List<Document> findAll(Pageable pageable, Sort sort);

    @Modifying
    @Query("update Document d set d.title =concat(d.title , ?1)")
    void updateDocumentTitle(String prefix);

    List<DocumentTitleOnly> findByTitleEndsWith(String ends);
}
