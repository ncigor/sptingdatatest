package com.spd.jpa;

import com.spd.entity.Document;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.RepositoryDefinition;

/**
 * Created by Igor on 16.12.2016.
 */
@RepositoryDefinition(domainClass = Document.class, idClass = String.class)
public interface JpaSpecificationRepository extends JpaSpecificationExecutor<Document> {

}
