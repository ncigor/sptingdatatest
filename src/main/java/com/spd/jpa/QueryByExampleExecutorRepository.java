package com.spd.jpa;

import com.spd.entity.Author;
import org.springframework.data.repository.RepositoryDefinition;
import org.springframework.data.repository.query.QueryByExampleExecutor;

/**
 * Created by Igor on 17.12.2016.
 */
@RepositoryDefinition(domainClass = Author.class, idClass = Integer.class)
public interface QueryByExampleExecutorRepository extends QueryByExampleExecutor<Author> {

}
