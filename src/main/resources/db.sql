SET FILES LOG true;
SET DATABASE EVENT LOG LEVEL 3;
SET DATABASE EVENT LOG SQL LEVEL 3;


-- Dumping structure for таблиця springdatatest.AUTHOR
CREATE TABLE AUTHOR (
  ID INTEGER NOT NULL, -- GENERATED ALWAYS AS IDENTITY ,
  name varchar(50) NOT NULL,
  PRIMARY KEY (ID)
);


-- Dumping structure for таблиця springdatatest.DOCUMENT
CREATE TABLE DOCUMENT (
  ID varchar(50) NOT NULL,
  title varchar(100) NOT NULL,
  author_id INTEGER DEFAULT NULL,
  text varchar(1000) NOT NULL,
  PRIMARY KEY (ID)
);
