INSERT INTO PUBLIC.AUTHOR (ID, name) VALUES
	(1, 'Igor'),
	(2, 'Oleg'),
	(3, 'Dmitrii');
/*!40000 ALTER TABLE AUTHOR ENABLE KEYS */;

-- Dumping data for table springdatatest.DOCUMENT: ~15 rows (приблизно)
/*!40000 ALTER TABLE DOCUMENT DISABLE KEYS */;
INSERT INTO DOCUMENT (ID, title, author_id, text) VALUES
	('402881e758eee2620158eee3a44b000f', 'Document -1494869712', 2, 'Document author : Oleg'),
	('402881e758eee2620158eee3a44c0010', 'Document 848870496', 2, 'Document author : Oleg'),
	('402881e758eee2620158eee3a44c0011', 'Document -1909160342', 2, 'Document author : Oleg'),
	('402881e758eee2620158eee3a44d0012', 'Document 782442443', 2, 'Document author : Oleg'),
	('402881e758eee2620158eee3a44d0013', 'Document -459872339', 2, 'Document author : Oleg'),
	('402881e758eee2620158eee3a44d0014', 'Document 105950844', 2, 'Document author : Oleg'),
	('402881e758eee2620158eee3a44e0015', 'Document -573018517', 2, 'Document author : Oleg'),
	('402881e758eee2620158eee3a44e0016', 'Document -1319346564', 2, 'Document author : Oleg'),
	('402881e758eee2620158eee3a44e0017', 'Document -1080199346', 2, 'Document author : Oleg'),
	('402881e758eee2620158eee3a44e0018', 'Document 1305893462', 2, 'Document author : Oleg'),
	('402881e758eee2620158eee3a45f0019', 'Document 759552948', 2, 'Document author : Oleg'),
	('402881e758eee2620158eee3a45f001a', 'Document 2046170556', 3, 'Document author : Oleg'),
	('402881e758eee2620158eee3a45f001b', 'Document -117326268', 2, 'Document author : Oleg'),
	('402881e758eee2620158eee3a45f001c', 'Document 1258344441', 3, 'Document author : Oleg'),
	('402881e758eee2620158eee3a45f001d', 'Document -1104034459', 2, 'Document author : Oleg');
/*!40000 ALTER TABLE DOCUMENT ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
